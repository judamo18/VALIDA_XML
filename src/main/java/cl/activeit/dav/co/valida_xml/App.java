package cl.activeit.dav.co.valida_xml;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Hello world!
 *
 */
public class App {

    private static final Logger logger = LogManager.getLogger(App.class.getName());
    private static String user = "juan";
    private static String lastname = "Montoya";

    public static void main(String[] args) {
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("Logging in user " + user + " with birthday " + lastname);
            }
            logger.info("Logging in user {} with birthday {}", user, lastname);

            logger.info("INICIANDO:.");
            logger.warn("INICIANDO:.");

            logger.trace("trazando ando");
            //System.out.print("**** " + LogManager.ROOT_LOGGER_NAME);

            Logger logger = LogManager.getRootLogger();

            Logger x = LogManager.getLogger("wombat");

            String response = "<persona><nombre>emm</nombre><apellido /></persona>";
            File file = new File("datos.xml");

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            StringReader reader = new StringReader(response);
            InputSource is = new InputSource(reader);
            byte[] barray = reader.toString().getBytes(StandardCharsets.UTF_8);
            //Document doc = dBuilder.parse(file);
            Document document = dBuilder.parse(is);

        } catch (ParserConfigurationException ex) {
            //Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            //Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            //Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
